import React from "react";
import { Routes, Route } from "react-router-dom";

//Components
import NavBar from "../components/navBar/NavBar";

//Views
import SearchProduct from "../views/searchProduct/SearchProduct";
import ProductDetail from "../views/productDetail/ProductDetail";

const Router = () => {
  return (
    <div>
      <NavBar />
      <Routes>
        <Route path="/" element={<></>} />
        <Route path="/items" element={<SearchProduct/>}/>
        <Route path="/items/:id" element={<ProductDetail/>}/>
      </Routes>
    </div>
  );
};

export default Router;
