export const condition = (condition: string | undefined) => {
    if(condition === 'new'){
        return 'Nuevo';
    } else if (condition === 'used'){
        return 'Usado';
    }
};