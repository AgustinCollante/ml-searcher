
export interface Author {
    name: string;
    lastname: string;
}


export interface Price {
    currency: string;
    amount: number;
    decimals: number;
}

export interface Product {
    author: Author;
    id: string;
    title: string;
    price: Price;
    picture: string;
    condition: string;
    free_shipping: boolean;
}

export interface Products {
    author: Author;
    items: Array<Product>;
    categories: Array<string>;
}

export interface ProductWithDescription extends Product {
    sold_quantity: number;
    description: string;
    categories: Array<string>;
}

export interface UseFetchProps {
    type: string;
    query?: string | undefined | null;
    id?: string;
}

export interface ProductProps{
    product: Product
}

export interface ProductDetailsProps{
    product: ProductWithDescription
}

export interface BreadcrumbsProps{
    categories: Array<string>
}

export interface ProductsContainerProps{
    children: any
  }
