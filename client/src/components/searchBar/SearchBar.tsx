import { useState } from "react";

//Hooks
import { useNavigate } from "react-router-dom";

//Styles
import "./searchBar.css";
import { AiOutlineSearch } from "react-icons/ai";

const SearchBar = () => {
  const [query, setQuery] = useState<string>("");

  const navigate = useNavigate();

  const handleClick = (e: any) => {
    e.preventDefault();
    query && navigate(`/items?search=${query}`);
    setQuery('');
  };

  return (
    <form className="searchBar" onSubmit={(e) => handleClick(e)}>
      <input
        className="searchBar__input"
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        placeholder={'Buscar productos'}
      />
      <button className="searchBar__button" type="submit">
        <AiOutlineSearch fontSize={24} />
      </button>
    </form>
  );
};

export default SearchBar;
