//Styles
import './button.css';

const Button = ({children}: any) => {
  return (
    <div className='button'>
        {children}
    </div>
  )
}

export default Button