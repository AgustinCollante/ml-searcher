//Hooks
import { useNavigate } from 'react-router-dom';

//Components
import SearchBar from '../searchBar/SearchBar';

//Styles
import './navBar.css';
import { logo } from '../../assets/images';



const NavBar = () => {
  const navigate = useNavigate();
  return (
    <header className='navBar'>
        <div className='logo' onClick={() => navigate('/', {replace: true})}>
            <img src={logo} alt='logo' style={{width:'100%'}}/>
        </div>
        <SearchBar/>
    </header>
  )
}

export default NavBar