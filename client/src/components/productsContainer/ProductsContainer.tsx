//Styles
import './productsContainer.css'

//Utils
import { ProductsContainerProps } from '../../utils/interfaces'

const ProductsContainer = ({children}: ProductsContainerProps) => {
  return (
    <div className='productsContainer'>
      {children}
    </div>
  )
}

export default ProductsContainer