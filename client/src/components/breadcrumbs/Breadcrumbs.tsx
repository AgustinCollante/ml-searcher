//Styles
import './Breadcrumbs.css';

//Utils
import { BreadcrumbsProps } from '../../utils/interfaces';

const Breadcrumbs = ({categories}: BreadcrumbsProps) => {
  return (
    <div className='breadcrumbs'>
      {categories && categories.map((e,i) => (
        <div key={i}>
          <span>{e}</span>
          <span style={{margin:'0 5px 0 5px'}}>{ i === categories.length -1 ? '' : ' > '}</span>
        </div>
      ))}
    </div>
  )
}

export default Breadcrumbs;