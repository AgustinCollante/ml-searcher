//Hooks
import { useNavigate } from "react-router-dom";

//Styles
import './product.css';

//Utils
import { ProductProps } from "../../utils/interfaces";
import { condition } from "../../utils/functions";



const Product = ({product}:ProductProps) => {
  const navigate = useNavigate();

  let price = new Intl.NumberFormat('es-AR');


  const handleClick = (e:any) => {
    e.preventDefault();
    navigate(`/items/${product.id}`);
  }
  
  return (
    <div className="product">
      <div style={{display: 'flex'}}>
        <div className="product__imgContainer" onClick={(e) => handleClick(e)}>
          <img className="product__img" src={product.picture} alt="product_image"/>
        </div>
        <div className="product__textsContainer" onClick={(e) => handleClick(e) }>
          <h3 className="product__title">$ {price.format(product.price.amount + (product.price.decimals/100))}</h3>
          <span className="product__description">{product.title}</span>
        </div>
      </div>
      <div className="product__location">
        <span>{condition(product.condition)}</span>
      </div>
    </div>
  );
};

export default Product;
