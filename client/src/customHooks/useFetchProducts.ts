import { useState, useEffect } from 'react';
import axios from "axios";

import { Products, ProductWithDescription, UseFetchProps } from '../utils/interfaces';

export const useFetch = ({ type, query, id } : UseFetchProps) => {
    const [product, setProduct] = useState<ProductWithDescription>();
    const [products, setProducts] = useState<Products>();

    useEffect(() => {
        ( async () => {
            if(type === 'query' &&  query){
                let { data } = await axios.get(`http://localhost:3001/api/items?q=${query}`);
                setProducts(data);
            } else if(type === 'id' && id) { 
                let { data }  = await axios.get(`http://localhost:3001/api/items/${id}`);
                setProduct(data);
            }
        })()
    },[query, id])
  return { product, products }
}