//Hooks
import { useParams } from "react-router-dom";
import { useFetch } from "../../customHooks/useFetchProducts";

//Components
import ProductsContainer from "../../components/productsContainer/ProductsContainer";
import Button from "../../components/button/Button";
import Breadcrumbs from "../../components/breadcrumbs/Breadcrumbs";

//Styles
import "./productDetail.css";

//Utils
import { condition } from "../../utils/functions";


const ProductDetail = () => {
  const { id } = useParams();
  let price = new Intl.NumberFormat('es-AR');

  const response = useFetch({type:'id', id});

  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        flexDirection:'column',
        alignItems:'center',
        justifyContent: "center",
        marginTop: "25px",
      }}
    >
      <Breadcrumbs categories={response.product?.categories || []}/>
      <ProductsContainer>
        <div className="productDetail">
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div className="productDetail__logoContainer">
              <img src={response.product?.picture} className="productDetail__logo" alt="image_detail" />
            </div>
            <div className="productDetail__dataContainer">
              <span className="productDetail__stateAndSell">
                {condition(response.product?.condition)} | {response.product?.sold_quantity} vendidos
              </span>
              <span className="productDetail__title">
                {response.product?.title}
              </span>
              <div style={{display:'flex'}}>
              <span className="productDetail__price">$ {price.format(response.product?.price.amount ? response.product.price.amount : 0)}</span>
              <span>{response.product?.price.decimals ? response.product.price.decimals : '00'}</span>
              </div>
              <Button>Comprar</Button>
            </div>
          </div>
          <div className="productDetail__containerDescription">
            <span className="productDetail__title">Descripcion del producto</span>
            <span className="productDetail__description">
              {response.product?.description}
            </span>
          </div>
        </div>
      </ProductsContainer>
    </div>
  );
};

export default ProductDetail;
