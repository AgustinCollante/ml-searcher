import { useEffect, useState } from 'react';

//Hooks
import { useSearchParams } from 'react-router-dom';
import { useFetch } from '../../customHooks/useFetchProducts';


//Components
import ProductsContainer from '../../components/productsContainer/ProductsContainer';
import Product from '../../components/product/Product';
import Breadcrumbs from '../../components/breadcrumbs/Breadcrumbs';

//Styles
import './searchProduct.css';


const SearchProduct = () => {
  const [query, setQuery] = useState<string | null>('')
  const [searchParams] = useSearchParams();

  useEffect(() => {
    let query = searchParams.get('search');
    query && setQuery(query);
  },[searchParams.get('search')])

  const response = useFetch({type: 'query', query: query})

  return (
    <div className='searchProduct'>
      <Breadcrumbs categories={response.products?.categories || []} />
      <ProductsContainer>
        { response.products ? response.products.items.map((e,i) => <Product key={i} product={e} />) : null }
      </ProductsContainer>
    </div>
  )
}

export default SearchProduct