const express = require("express");
const cors = require("cors");
const productsRoute = require('./routes/products');

const app = express();

app.use(express.json({limit: "25mb"}));
app.use(express.urlencoded({limit: "25mb"}));
app.use(cors());

app.use("/api", productsRoute );

app.listen(3001)