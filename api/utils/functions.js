const mostCommonCategory = (categories) => {
    let counterCategories = categories.reduce((cnt, cur) => (cnt[cur] = cnt[cur] + 1 || 1, cnt),{});
    let category;
    for (const key in counterCategories) {
        if(category){
            if(counterCategories[key] > counterCategories[category]){
                category = key;
            } 
        } else {
            category = key;
        }
    }
    return category;
};


module.exports = {
    mostCommonCategory
}