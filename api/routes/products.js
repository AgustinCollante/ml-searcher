const { Router, query } = require("express");
const axios = require('axios');
const { mostCommonCategory } = require('../utils/functions');

const router = Router();

router.get('/items', async (req, res) => {
    try {
        let { q } = req.query;
        let { data } = await axios.get(`https://api.mercadolibre.com/sites/MLA/search?q=${q}&limit=4`);
        if(data.results?.length === 0) return res.status(404).json({error: true, message: 'No se encontraron productos'});
        let categories = [];
        let items = data?.results?.map(e => {
            let product = {
                id: e.id,
                title: e.title,
                price:{
                    currency: e.currency_id,
                    amount: Number(e.price.toString().split('.')[0]),
                    decimals: Number.isNaN(Number(e.price.toString().split('.')[1])) ? 0 : Number(e.price.toString().split('.')[1])
                },
                picture: e.thumbnail,
                condition: e.condition,
                free_shipping: e.shipping?.free_shipping,
            }
            categories.push(e.category_id);
            return product;
        });
        let category = mostCommonCategory(categories);
        let response = await axios.get(`https://api.mercadolibre.com/categories/${category}`)
        categories = response.data?.path_from_root.map(e => {
            return e.name;
        });
        res.status(200).json({author: { name:"Agustin", lastname: "Collante"}, items, categories})
    } catch (error) {
        if(error?.response?.status === 404){
            return res.status(404).json({error: true, message: 'No se encontraron productos'});
        } else {
            console.log(error)
            return res.status(500).json({error: true, message: 'Ocurrio un problema'});
        }
    }
})

router.get('/items/:id', async (req, res) => {
    try {
        let { id } = req.params;
        let { data } = await axios.get(`https://api.mercadolibre.com/items/${id}`);
        let description = await axios.get(`https://api.mercadolibre.com/items/${id}/description`)
        let product = {
                id: data.id,
                title: data.title,
                price:{
                    currency: data.currency_id,
                    amount: Number(data.price.toString().split('.')[0]),
                    decimals: Number.isNaN(Number(data.price.toString().split('.')[1])) ? 0 : Number(data.price.toString().split('.')[1])
                },
                picture: data.pictures[0]?.url,
                condition: data.condition,
                free_shipping: data.shipping?.free_shipping,
                sold_quantity: data.sold_quantity,
                description: description?.data?.plain_text,
            }
        let response = await axios.get(`https://api.mercadolibre.com/categories/${data.category_id}`)
        let categories = response.data?.path_from_root.map(e => {
            return e.name;
        });
        res.status(200).json({author: { name:"Agustin", lastname: "Collante"}, ...product, categories})
    } catch (error) {
        if(error?.response?.status === 404){
            return res.status(404).json({error: true, message: 'No se encontro el producto'});
        } else {
            console.log(error)
            return res.status(500).json({error: true, message: 'Ocurrio un problema'});
        }
    }
})

module.exports = router;